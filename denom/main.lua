require("anal")
require("maplib/maplib")
require("yalt/yalt")
require("menu/menu")
require("playerlib/playerlib")
require("npclib/npclib")
require("msglib/msglib")
require("guilib/guilib")
-- require("monsterlib/monsterscript")
-- require("monsterlib/monsterlib")
require("magiclib/magiclib")
require("inventorylib/inventorylib")
require("winzone/winzone")
require("entitylib/entitylib")
require("debug/debug")

math.randomseed(os.time())

function love.load( arg )
  state = arg[2]
  if not state then
    print("No state argument. run \"./start.sh <lib>\"")
    state = "menu"
  end
  print("Starting with `"..state.."` state.\n")
  maplib.load( arg )
  menu:toggle()
  menu_view = {}
  menu_view[1] = {
    title = "Denom",
    desc = "The 2D Fantasy RPG",
    {t="New Game",cb="ng"},
    {t="Options",cb="op"},
    {t="Exit",cb="exit"}
  }
  menu_view[2] = {
    title="Options",
    desc="Set your options here.",
    {t="Fullscreen",cb="fs"},
    {t="Return",cb="mm"}
  }
  menu_view[3] = {
    title="Quit",
    desc="Are you sure you want to quit?",
    {t="Confirm",cb="cexit"},
    {t="Return",cb="mm"}
  }
	song = love.audio.newSource("shared_assets/AncientForest.ogg", "loop")
	--love.audio.play(song)  THIS WILL PLAY THE SONG
	debug.load()
	menu:load(menu_view)
  playerlib.load()
  entitylib.load()
	winzone.load()
  msglib.load()
  -- castlescript.load()
  npclib.load()
  magiclib.load()
	inventorylib.load()
  guilib.load();
end

function love.draw()
  if state == "maplib" then
    maplib.draw()
    entitylib.draw()
    playerlib.draw()
		winzone.draw()
    guilib.draw()
    npclib.draw()
    -- monsterlib.draw()
    magiclib.draw()
  elseif state == "menu" then
    menu:draw()
  else
    state = "<Invalid>"
  end
  yalt:draw()
  love.graphics.print("Current State: "..state,0,0)
end

function love.update(dt)
  if state == "maplib" then
    maplib.update(dt) 
    playerlib.update(dt)
		winzone.update(dt)
		entitylib.update(dt)
		debug.update(dt)
    -- monsterlib.update(dt)
    guilib.update(dt)
    magiclib.update(dt)
  elseif state == "menu" then
    menu:update(dt)
  end
end

function love.keypressed( key, unicode )
  yalt:keypressed(key,unicode)
  if state == "maplib" then
    playerlib.keypressed(key,unicode)
    npclib.keypressed(key,unicode)
    magiclib.keypressed(key,unicode)
  elseif state == "menu" then
    menu:keypressed(key)  
  end
end

function love.keyreleased( key, unicode )
  yalt:keyreleased(key,unicode)
  if state == "maplib" then
    playerlib.keyreleased(key,unicode)
  end
end

function love.mousepressed( x, y, button )
	if state == "menu" then
		menu:mousepressed(x,y,button)
	elseif state == "maplib" then
		msglib.mousepressed(x, y, button)
                magiclib.mousepressed(x, y, button)
	end
end

function love.mousereleased( x, y, button )
end

function love.quit()
  print("Thanks for playing!")
end

function yalt:callback(line)
  local arr = explode(" ",line)
  if arr[1] == "maplib" then
    maplib.yalt_callback(arr)
  elseif arr[1] == "playerlib" then
    playerlib.yalt_callback(arr)
  end
  yalt:push(line)
end

function explode(div,str) -- credit: http://richard.warburton.it
  if (div=='') then return false end
  local pos,arr = 0,{}
  -- for each divider found
  for st,sp in function() return string.find(str,div,pos,true) end do
    table.insert(arr,string.sub(str,pos,st-1)) -- Attach chars left of current divider
    pos = sp + 1 -- Jump past current divider
  end
  table.insert(arr,string.sub(str,pos)) -- Attach chars right of last divider
  return arr
end

function menu:callback(cb)
  if cb == "ng" then
    menu:toggle()
    state = "maplib"
  elseif cb == "op" then
    menu:setstate(2)
  elseif cb == "exit" then
    menu:setstate(3)
  elseif cb == "cexit" then
    love.event.push("q")
  elseif cb == "fs" then
    love.graphics.toggleFullscreen( )
  elseif cb == "mm" then
    menu:setstate(1)
  else
    print("unknown command:"..cb)
  end
end

-- Print anything - including nested tables
function table_print (tt, indent, done) --http://lua-users.org/wiki/TableSerialization
  done = done or {}
  indent = indent or 0
  if type(tt) == "table" then
    for key, value in pairs (tt) do
      io.write(string.rep (" ", indent)) -- indent it
      if type (value) == "table" and not done [value] then
        done [value] = true
        io.write(string.format("[%s] => table\n", tostring (key)));
        io.write(string.rep (" ", indent+4)) -- indent it
        io.write("(\n");
        table_print (value, indent + 7, done)
        io.write(string.rep (" ", indent+4)) -- indent it
        io.write(")\n");
      else
        io.write(string.format("[%s] => %s\n",
            tostring (key), tostring(value)))
      end
    end
  else
    io.write(tt .. "\n")
  end
end
