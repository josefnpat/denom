winzone = {}

function winzone.load()
	winzone.image = love.graphics.newImage("shared_assets/winzone.png")
	winzone.capture = love.graphics.newImage("shared_assets/captured.png")	
	winzone.time = 200
	winzone.x = 400
	winzone.y = 400
end

function winzone.update(dt)
	distance = math.sqrt(math.abs(playerlib.x-(maplib.tx + winzone.x + 50))^2 + math.abs(playerlib.y-(maplib.ty + winzone.y + 50))^2)	
	if distance < 50 then 	
		winzone.time = winzone.time - 1
	end
end

function winzone.draw()
		love.graphics.draw(winzone.image, maplib.tx + winzone.x, maplib.ty + winzone.y)
		--love.graphics.line(maplib.tx + winzone.x, maplib.ty + winzone.y,384,280) -- DEBUG
		if winzone.time < 1 then love.graphics.draw(winzone.capture, winzone.x - 20, winzone.y - 20) end
end

