inventorylib = {}

function inventorylib.load()
	 inventorylib.empty()
end

function inventorylib.adjust_item(item, x)
	 local stock
	 x = x or 1
	 
	 stock = inventorylib.inventory[item] or 0
	 stock = stock + x
	 if stock < 0 then
			stock = 0
	 end
	 inventorylib.inventory[item] = stock
end

function inventorylib.add_item(item, x)
	 x = x or 1
	 inventorylib.adjust_item(item, x)
end

function inventorylib.remove_item(item, x)
	 x = x or -1
	 inventorylib.adjust_item(item, x)
end

function inventorylib.empty()
	 inventorylib.inventory = {}
end

function inventorylib.in_stock(item)
	 return inventorylib.inventory[item] or 0
end

function inventorylib.set_item_stock(item, stock)
	 inventorylib.inventory[item] = stock
end