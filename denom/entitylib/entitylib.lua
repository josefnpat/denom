entitylib = {}

function entitylib.load()
  entitylib.data = {}
  entitylib.img = love.graphics.newImage("entitylib/enemy.png")
  entitylib.new_zombie_dt = 0
  
  entitylib.img_north = love.graphics.newImage("entitylib/north.png")
  entitylib.img_south = love.graphics.newImage("entitylib/south.png")
  entitylib.img_east = love.graphics.newImage("entitylib/east.png")
  entitylib.img_west = love.graphics.newImage("entitylib/west.png")


end

function entitylib.random_zombie()
  entitylib.new(math.random(16,128-16)*32,math.random(16,128-16)*32)
end

function entitylib.dist(x1,y1,x2,y2)
  return math.sqrt( (x1-x2)^2 + (y1-y2)^2 )
end

function entitylib.draw()
  for _,v in ipairs(entitylib.data) do
  
  
  if v.state == 1 then 
    v.ani_north:draw(v.x-16, v.y-16) 
  elseif playerlib.state == 2 then 
    v.ani_south:draw(v.x-16, v.y-16)
  elseif playerlib.state == 3 then 
    v.ani_west:draw(v.x-16, v.y-16)
  elseif playerlib.state == 4 then
    v.ani_east:draw(v.x-16, v.y-16)
  end 
  
    love.graphics.draw(entitylib.img,v.x+maplib.tx,v.y+maplib.ty,0,1,1,16,16)
--    love.graphics.rectangle("line",v.x+maplib.tx-16,v.y+maplib.ty-16,32,32)
  end
end

function entitylib.checkpath(range,x1,y1,x2,y2)
  local res = range/16
  local xseg,yseg = (x2-x1)/res,(y2-y1)/res
  local xt,yt = x1+xseg,y1+yseg
  for i = 1,res do
    tilex,tiley = maplib.attile(xt-maplib.tx,yt-maplib.ty)
    if maplib.cmap[tilex] and maplib.cmap[tilex][tiley] then
      return false
    end
    xt = xt + xseg
    yt = yt + yseg
  end
  return true
end

function entitylib.update(dt)

  for i,v in ipairs(entitylib.data) do
    if v.move == 1 then
      v.ani_north:play()
      v.ani_south:play()
      v.ani_east:play()
      v.ani_west:play()
    else
      v.ani_north:stop()
      v.ani_south:stop()
      v.ani_east:stop()
      v.ani_west:stop()
    end
    v.ani_north:update(dt) 
    v.ani_south:update(dt)
    v.ani_east:update(dt)
    v.ani_west:update(dt)
  end

  entitylib.new_zombie_dt = entitylib.new_zombie_dt + dt
  if entitylib.new_zombie_dt > 0.2 then
    entitylib.new_zombie_dt = 0
    entitylib.random_zombie()
  end

  for i,v in ipairs(entitylib.data) do
    local route = entitylib.routetoplayer(v)
    if route == "direct" then
      v:run(dt,playerlib.x-maplib.tx,playerlib.y-maplib.ty)
    elseif route == "arc1" then
      v:run(dt,playerlib.x-maplib.tx,v.y)
    elseif route == "arc2" then
      v:run(dt,v.x,playerlib.y-maplib.ty)
    end
  end
end

function entitylib.routetoplayer(v)
  if entitylib.dist(playerlib.x-maplib.tx,playerlib.y-maplib.ty,v.x,v.y) < v.range then
    local x1,y1,x2,y2 = playerlib.x,playerlib.y,v.x+maplib.tx,v.y+maplib.ty
    if entitylib.checkpath(v.range,playerlib.x,playerlib.y,v.x+maplib.tx,v.y+maplib.ty) then -- Direct
      return "direct"
    elseif entitylib.checkpath(v.range,x1,y2,x2,y2) and entitylib.checkpath(v.range,x1,y1,x1,y2) then -- First Arc
      return "arc1"
    elseif entitylib.checkpath(v.range,x2,y1,x2,y2) and entitylib.checkpath(v.range,x1,y1,x2,y1) then -- Second Arc
      return "arc2"
    end
  end
end

function entitylib.new(x,y)
  local ent = {}
  ent.x = x
  ent.y = y
  ent.hp = math.random(15,25)
  ent.range = math.random(400,1200)
  ent.speed = math.random(140,180)
  ent.skip = false
  
  ent.ani_north = newAnimation(entitylib.img_north, 32, 32, 0.2, 3)
  ent.ani_south = newAnimation(entitylib.img_south, 32, 32, 0.2, 3)
  ent.ani_east = newAnimation(entitylib.img_east, 32, 32, 0.2, 3)
  ent.ani_west = newAnimation(entitylib.img_west, 32, 32, 0.2, 3)

  ent.ani_north:setMode("loop") 
  ent.ani_south:setMode("loop")
  ent.ani_east:setMode("loop")
  ent.ani_west:setMode("loop")
  
  ent.run = function(self,dt,tx,ty)
    local direction = math.atan2(tx-self.x,self.y-ty)+math.pi/2
    newx = self.x-(math.cos(direction)*dt*self.speed)
    newy = self.y-(math.sin(direction)*dt*self.speed)
    for i,v in ipairs(entitylib.data) do
      if entitylib.dist(self.x,self.y,v.x,v.y) < 32 then
        self.skip = true
        v.skip = false
      end
    end
    if not self.skip then
      self.x = newx
      self.y = newy
    else
      self.skip = false
    end
  end
  table.insert(entitylib.data,ent)
end
