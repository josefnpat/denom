-- w=96 h=128
-- playersize=32
-- frames=12
playerlib = {}
playerlib.db = false
function playerlib.load()
  playerlib.x = 400
  playerlib.y = 300
  playerlib.move = 0
  playerlib.state = 2 -- POKEMON REFRENCE MUCH?
  
  playerlib.sword = love.graphics.newImage("playerlib/sword.png")
  playerlib.width = playerlib.sword:getWidth()
  playerlib.height = playerlib.sword:getHeight()
  
  playerlib.img_north = love.graphics.newImage("playerlib/north.png")
  playerlib.img_south = love.graphics.newImage("playerlib/south.png")
  playerlib.img_east = love.graphics.newImage("playerlib/east.png")
  playerlib.img_west = love.graphics.newImage("playerlib/west.png")

  playerlib.ani_north = newAnimation(playerlib.img_north, 32, 32, 0.2, 3)
  playerlib.ani_south = newAnimation(playerlib.img_south, 32, 32, 0.2, 3)
  playerlib.ani_east = newAnimation(playerlib.img_east, 32, 32, 0.2, 3)
  playerlib.ani_west = newAnimation(playerlib.img_west, 32, 32, 0.2, 3)

  playerlib.ani_north:setMode("loop") 
  playerlib.ani_south:setMode("loop")
  playerlib.ani_east:setMode("loop")
  playerlib.ani_west:setMode("loop")
end

function playerlib.draw()
  love.graphics.print("move:"..playerlib.move,0,16)
  if playerlib.state == 1 then 
    playerlib.ani_north:draw(playerlib.x-16, playerlib.y-16) 
  elseif playerlib.state == 2 then 
    playerlib.ani_south:draw(playerlib.x-16, playerlib.y-16)
  elseif playerlib.state == 3 then 
    playerlib.ani_west:draw(playerlib.x-16, playerlib.y-16)
  elseif playerlib.state == 4 then
    playerlib.ani_east:draw(playerlib.x-16, playerlib.y-16)
  end 
end

function playerlib.update(dt)
  for i,v in ipairs(entitylib.data) do
    if entitylib.dist(playerlib.x-maplib.tx,playerlib.y-maplib.ty,v.x,v.y) < 32 then
      v.hp = v.hp - 1
      if v.hp <= 0 then
        table.remove(entitylib.data,i)
      end
      guilib.health = guilib.health - 1
      if guilib.health < 0 then
        guilib.health = 0
      end
    end
  end
  if playerlib.move == 1 then
    playerlib.ani_north:play()
    playerlib.ani_south:play()
    playerlib.ani_east:play()
    playerlib.ani_west:play()
  else
    playerlib.ani_north:stop()
    playerlib.ani_south:stop()
    playerlib.ani_east:stop()
    playerlib.ani_west:stop()
  end
  playerlib.ani_north:update(dt) 
  playerlib.ani_south:update(dt)
  playerlib.ani_east:update(dt)
  playerlib.ani_west:update(dt)
end

function playerlib.keypressed(key,unicode)
  playerlib.move = 0
  
  if love.keyboard.isDown("up") or love.keyboard.isDown("w") then
    playerlib.state = 1
    playerlib.move = 1
    playerlib.swing = 1
  end
  if love.keyboard.isDown("down") or love.keyboard.isDown("s") then
    playerlib.state = 2
    playerlib.move = 1
    playerlib.swing = 2
  end
  if love.keyboard.isDown("left") or love.keyboard.isDown("a") then
    playerlib.state = 3 
    playerlib.move = 1
    playerlib.swing = 3
  end
  if love.keyboard.isDown("right") or love.keyboard.isDown("d") then 
    playerlib.state = 4
    playerlib.move = 1
    playerlib.swing = 4
  end
end

function playerlib.keyreleased(key,unicode)
  playerlib.keypressed(key,unicode) --Recheck for state!
  if love.keyboard.isDown("up") or love.keyboard.isDown("down") or love.keyboard.isDown("left") or love.keyboard.isDown("right") or love.keyboard.isDown("w") or love.keyboard.isDown("s") or love.keyboard.isDown("a") or love.keyboard.isDown("d") then
    playerlib.move = 1
  else
    playerlib.move = 0
  end
end

function playerlib.yalt_callback(arr)
	if arr[2] == "debug" then
		debug.checkDebug()
	end
end
