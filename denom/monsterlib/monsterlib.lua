
monsterlib = {}

function monsterlib.load(x, y, state, face, mtype)
  -- monsterlib.load(x, y, state, level, name, face)
  monsterlib.x = x -- monsters x
  monsterlib.y = y -- monster y
  monsterlib.state = state -- 1 = Friendly,,,,2=Agrivated
  -- monsterlib.level = level -- 1-10 AKA the speed at which they think
  -- monsterlib.name = name -- The name of the monster
  monsterlib.face = face -- NORTH(1) SOUTH(2) EAST(3) OR WEST(4)
  monsterlib.alive = 1 -- alive or dead = 1 or 0
  monsterlib.mtype = mtype
  -- Handles which monster will be loaded...AKA ghost is the only one so far
  if monsterlib.mtype == "ghost" then
    monsterlib.img_ = love.graphics.newImage("monsterlib/ghost.png") -- loads the ghost png
    monsterlib.img_north = love.graphics.newImage("monsterlib/nghost.png") -- loads the NORTH ghost
    monsterlib.img_south = love.graphics.newImage("monsterlib/sghost.png") -- loads the NORTH ghost
    monsterlib.img_east = love.graphics.newImage("monsterlib/eghost.png") -- loads the NORTH ghost
    monsterlib.img_west = love.graphics.newImage("monsterlib/wghost.png") -- loads the NORTH ghost
    -- monsterlib.img_north = love.graphics.newQuad(1, 92, 96, 32, 96, 128)	-- create the NORTH quad
    -- monsterlib.img_south = love.graphics.newQuad(1, 1, 96, 32, 96, 128) -- create the SOUTH quad
    -- monsterlib.img_east = love.graphics.newQuad(1, 64, 96, 32, 96, 128) -- create the EAST quad
    -- monsterlib.img_west = love.graphics.newQuad(1, 32, 96, 32, 96, 128) -- create the WEST quad
    
    monsterlib.ani_north = newAnimation(monsterlib.img_north, 32, 32, 0.2, 3) -- create the NORTH animation
    monsterlib.ani_south = newAnimation(monsterlib.img_south, 32, 32, 0.2, 3) -- create the SOUTH animation
    monsterlib.ani_east = newAnimation(monsterlib.img_east, 32, 32, 0.2, 3) -- create the EAST animation
    monsterlib.ani_west = newAnimation(monsterlib.img_west, 32, 32, 0.2, 3) -- create the WEST animation
    
    monsterlib.ani_north:setMode("loop")
    monsterlib.ani_south:setMode("loop")
    monsterlib.ani_south:setMode("loop")
    monsterlib.ani_west:setMode("loop")
  end
end

function monsterlib.draw()
  if monsterlib.face == 1 then -- NORTH NORTH NORTH
    monsterlib.ani_north:draw(monsterlib.x, monsterlib.y)
  elseif monsterlib.face == 2 then -- SOUTH SOUTH SOUTH
    monsterlib.ani_south:draw(monsterlib.x, monsterlib.y)
  elseif monsterlib.face == 3 then -- EAST EAST EAST
    monsterlib.ani_east:draw(monsterlib.x, monsterlib.y)
  elseif monsterlib.face == 4 then -- WEST WEST WEST
    monsterlib.ani_west:draw(monsterlib.x, monsterlib.y)
  end
end

function monsterlib.update(dt)
  if monsterlib.alive == 1 then
    if monsterlib.state == 1 then -- nice
       
    end
    if monsterlib.state == 2 then -- aggrivated
      if playerlib.x < monsterlib.x then 
        monsterlib.x = monsterlib.x - (100*dt)
        monsterlib.face = 4
      elseif playerlib.x > monsterlib.x then 
        monsterlib.x = monsterlib.x + (100*dt)
        monsterlib.face = 3
      elseif playerlib.y < monsterlib.y then 
        monsterlib.y = monsterlib.y - (100*dt)
        monsterlib.face = 1
      elseif playerlib.y > monsterlib.y then 
        monsterlib.y = monsterlib.y + (100*dt)
        monsterlib.face = 2
      end
    end
  end
end


