require("monsterlib/monsterlib")

castlescript = {}

function castlescript.load()
  monsterlib.load(100, 100, 2, 2, "ghost")
  monsterlib.load(150, 150, 2, 2, "ghost")
  monsterlib.load(200, 200, 2, 2, "ghost")
  monsterlib.load(250, 250, 2, 2, "ghost")
  monsterlib.load(300, 300, 2, 2, "ghost")
  monsterlib.load(350, 350, 2, 2, "ghost")
  monsterlib.load(400, 400, 2, 2, "ghost")
end
