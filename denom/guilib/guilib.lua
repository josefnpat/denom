guilib = {}
function guilib.load( arg )
  guilib.img_empty = love.graphics.newImage("guilib/assets/empty.png")
  guilib.img_web20 = love.graphics.newImage("guilib/assets/web20.png")
  guilib.mana = 100; 
  guilib.max_mana = 100;
  guilib.health = 100;
  guilib.max_health = 200;
  guilib.mana_time = 0
  guilib.mana_rate = 0.1
  guilib.health_time = 0
  guilib.health_rate = 0.1
end

function guilib.draw()
  love.graphics.print("Hello World - guilib",0,16)
  guilib.draw_inv()
  guilib.draw_health()
  guilib.draw_mana()
end

function guilib.draw_inv()
  for i = 0 ,3 do
    for j = 1 ,4 do
      love.graphics.draw(guilib.img_empty,i*36+4,600-j*36)
    end
  end
end

function guilib.draw_health()
  local act = (guilib.health / guilib.max_health ) * 256
  love.graphics.setColor( 0, 0, 0, 255 )
  love.graphics.rectangle("fill",4,4,256,32)
  love.graphics.setColor( 255, 0, 0, 255 )
  love.graphics.rectangle("fill",4,4,act,32)
  love.graphics.setColor( 255, 255, 255, 255 )
  love.graphics.draw(guilib.img_web20,4,4)
  love.graphics.printf("Health: "..guilib.health.." / "..guilib.max_health,4,4+8,256,"center")
end

function guilib.draw_mana()
  local act = (guilib.mana / guilib.max_mana ) * 256
  love.graphics.setColor( 0, 0, 0, 255 )
  love.graphics.rectangle("fill",4,4+32+4,256,32)
  love.graphics.setColor( 0, 0, 255, 255 )
  love.graphics.rectangle("fill",4,4+32+4,act,32)
  love.graphics.setColor( 255, 255, 255, 255 )
  love.graphics.draw(guilib.img_web20,4,4+32+4)
  love.graphics.printf("Mana: "..guilib.mana.." / "..guilib.max_mana,4,4+32+4+8,256,"center")
end

function guilib.update(dt)
  guilib.mana_time = guilib.mana_time + dt
  if guilib.mana_time > guilib.mana_rate then
    guilib.mana_time = 0
    guilib.mana = guilib.mana + 1
    if guilib.mana > guilib.max_mana then
      guilib.mana = guilib.max_mana
    end
  end
  guilib.health_time = guilib.health_time + dt
  if guilib.health_time > guilib.health_rate then
    guilib.health_time = 0
    guilib.health = guilib.health + 1
    if guilib.health > guilib.max_health then
      guilib.health = guilib.max_health
    end
  end
end

function guilib.keypressed( key, unicode )

end
function guilib.keyreleased( key )

end
function guilib.mousepressed( x, y, button )

end
function guilib.mousereleased( x, y, button )

end
function guilib.quit()

end
