debug = {}

function debug.load()
	debug.running = false 
end

function debug.update(dt)
	if debug.running == true then debug.run() end
end

function debug.run()
  yalt:push("Player X: "..maplib.tx + playerlib.x.." Player Y: "..maplib.ty + playerlib.y)
end

function debug.checkDebug()
	debug.running = not debug.running
end
