msglib = {}
function msglib.load(arg)
	msglib.img_bordertop = love.graphics.newImage("msglib/border.png")
	msglib.img_borderbottom = love.graphics.newImage("msglib/border2.png")
	msglib.img_borderleft = love.graphics.newImage("msglib/border3.png")
	msglib.img_borderright = love.graphics.newImage("msglib/border4.png")
	msglib.img_button = love.graphics.newImage("msglib/button.png")
	msglib.but_x, msglib.but_y = (love.graphics.getWidth()/2)+48,(love.graphics.getHeight()-32)
	drawMessage = false
end

function msglib.draw(arg)
	msglib.draw_message(arg)
	if drawMessage then
		msglib.contents, msglib.size = love.filesystem.read("npclib/convo.txt", 12)
		love.graphics.print(msglib.contents, (love.graphics.getWidth()/2)-112,love.graphics.getHeight()-80)
	end
end

function msglib.draw_message()
	love.graphics.setColor(128, 128, 128, 128)
	love.graphics.rectangle("fill",(love.graphics.getWidth()/2)-128,love.graphics.getHeight()-96,256,96)
	love.graphics.setColor(255, 255, 255, 255)
	love.graphics.draw(msglib.img_bordertop,(love.graphics.getWidth()/2)-128,love.graphics.getHeight()-96)
	love.graphics.draw(msglib.img_borderbottom,(love.graphics.getWidth()/2)-128,love.graphics.getHeight()-16)
	love.graphics.draw(msglib.img_borderleft,(love.graphics.getWidth()/2)-128,love.graphics.getHeight()-80)
	love.graphics.draw(msglib.img_borderright,(love.graphics.getWidth()/2)+112,love.graphics.getHeight()-80)
	love.graphics.draw(msglib.img_button,msglib.but_x,msglib.but_y)
end

function msglib.update(dt)
	
end
function msglib.keypressed( key, unicode )
	
end
function msglib.keyreleased( key )

end
function msglib.mousepressed( x, y, button )
	if button == "l" then
		if x > msglib.but_x and x < msglib.but_x + msglib.img_button:getWidth() and y > msglib.but_y and y < msglib.but_y + msglib.img_button:getHeight() then
			drawMessage = true
		end
   end
end
function msglib.mousereleased( x, y, button )

end
function msglib.quit()

end
