-- Init lib
maplib = {}
maplib.show_collision_debug = false
function maplib.load()

  -- ATL config
  maplib.loader = require("AdvTiledLoader.Loader")
  maplib.loader.path = "maplib/maps/" -- This is where our maps will be located
  maplib.map = maplib.loader.load("zombies.tmx") -- This is our overworld map
  
  -- The current offset for the map
  maplib.tx = 0
  maplib.ty = 0

  maplib.scale = 1 -- zoom size
  -- Generate the collision map
  maplib.cmap = maplib.gen_cmap()

  -- Count the number of collidable tiles
  local total = 0
  -- For each row do
  for i,v in ipairs(maplib.cmap) do
    -- Add the count of the index within the row (effectively the column)
    total = total + #v
  end
  print("Solid Tiles:"..total)
  -- debug oscillator
  maplib.debug_tile_dt = 1
end

-- draw hook
function maplib.draw()
  -- floor the values to avoid alpha bleeding
  local ftx, fty = math.floor(maplib.tx), math.floor(maplib.ty)

  -- push the info into the graphics system
  love.graphics.push()
  love.graphics.scale(scale)
  love.graphics.translate(ftx, fty)

  -- draw the maplib
  maplib.map:draw()

  -- pop the info off the graphics system
  love.graphics.pop()
  
  if maplib.show_collision_debug then
    -- For each tile in the collision map
    for x,v in pairs(maplib.cmap) do
      for y,w in pairs(v) do
        -- Set to red between 64 and 128 in a sinusoidal wave form from the debug oscillator
        love.graphics.setColor(255,0,0,63+64*math.sin(maplib.debug_tile_dt))
        -- draw the debug layer
        love.graphics.rectangle("fill",maplib.tx+(x-1)*32,maplib.ty+(y-1)*32,32,32)
        -- return the color back to normal
        love.graphics.setColor(255,255,255,255)
        -- print the location
        love.graphics.print(x..","..y,maplib.tx+(x-1)*32,maplib.ty+(y-1)*32)
      end
    end
    -- reset graphics (TODO: Find out if we need this.)
    love.graphics.setColor(255,255,255,255)
  end
  
  -- Find out where the player is on the map.
  local x,y = maplib.atplayer(maplib.tx,maplib.ty)
  love.graphics.print("Player: ("..x..","..y..")",0,200)
  -- fps
  love.graphics.printf(love.timer.getFPS(),0,0,love.graphics.getWidth(),"right")
end

-- Generate the collision map.
-- Be sure to run this once everytime there is a new map.
function maplib.gen_cmap()
  -- Identify the layer to be considered collidable
  local layer = map.tl["walls"]
  -- Init
  local cmap_data = {}
  -- for each tile
  for x,v in ipairs(layer.tileData) do
    for y,w in ipairs(v) do
      -- temp
      local tile
      -- if tile exists
      if layer.tileData[y] then
        -- get data
        tile = map.tiles[layer.tileData[y][x]]
      end
      -- validation
      if tile and tile.properties.Solid then
        -- check existing
        if not cmap_data[x] then
          cmap_data[x] = {}
        end
        -- assgn true
        cmap_data[x][y] = 1
      end
    end
  end
  return cmap_data
end

-- return the player location, relative to the map
function maplib.atplayer(mapx,mapy)
  return maplib.attile(playerlib.x-mapx,playerlib.y-mapy)
end

-- return a location from a pixel based input, relative to the map
function maplib.attile(x,y)
  return math.floor(x/32)+1, math.floor(y/32)+1
end

-- update hook
function maplib.update(dt)
  -- bitches don't know about my applying range to the map render
  maplib.map:setDrawRange(-maplib.tx+400-love.graphics.getWidth()/2,-maplib.ty+300-love.graphics.getHeight()/2,love.graphics.getWidth()*1.2,love.graphics.getHeight()*1.2)
  -- update oscillation
  maplib.debug_tile_dt = maplib.debug_tile_dt + dt*2
  if maplib.debug_tile_dt >= math.pi then
    maplib.debug_tile_dt = 0
  end
  -- temp input
  local x,y = maplib.tx, maplib.ty
  -- keyboard input and movement
  if love.keyboard.isDown("up") or love.keyboard.isDown("w") then
    y = y + 150*dt
  end
  if love.keyboard.isDown("down") or love.keyboard.isDown("s") then
    y = y - 150*dt
  end
  if love.keyboard.isDown("left") or love.keyboard.isDown("a") then
    x = x + 150*dt
  end
  if love.keyboard.isDown("right") or love.keyboard.isDown("d") then
    x = x - 150*dt
  end
  -- validation pre-render
  tilexa,tileya = maplib.atplayer(x+15,y+15)
  tilexb,tileyb = maplib.atplayer(x+15,y-15)
  tilexc,tileyc = maplib.atplayer(x-15,y+15)
  tilexd,tileyd = maplib.atplayer(x-15,y-15)
  -- validation
  if not ( maplib.cmap[tilexa] and maplib.cmap[tilexa][tileya] ) and
   not ( maplib.cmap[tilexb] and maplib.cmap[tilexb][tileyb] ) and
   not ( maplib.cmap[tilexc] and maplib.cmap[tilexc][tileyc] ) and
   not ( maplib.cmap[tilexd] and maplib.cmap[tilexd][tileyd] ) then
    -- valid move
    maplib.tx,maplib.ty = x,y
  end
end

-- yalt callback
function maplib.yalt_callback(arr)
  if arr[2] == "fullscreen" then
    love.graphics.toggleFullscreen( )
  end
end
