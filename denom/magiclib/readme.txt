Bolt Type ----- Speed -- Power -- Discription
Earth     -----   2   --   5   -- A rocky bolt of rock.
Water     -----   4   --   3   -- A watery bolt of water.
Fire      -----   4   --   5   -- A firey bolt of fire.
Air       -----   6   --   2   -- A windy bolt of air.
Lighting  -----   8   --   7   -- A lighting bolt.

Magic Traps --- Time  -- Power -- Discription
Entangle    ---  5    --   3   -- Vines entrap your enemy(s).
Sludge      ---  10   --   2   -- Slows your enemie(s) down.
Black Hole  ---  10   --   5   -- A gravity trap that keeps your enimies in one place.
Vortex      ---  5    --  10   -- Sucks your enimie(s) into a never ending vortex.

Extra
Heal 		--- Heals yourself
Telekenesis 	--- Use your mouse to slam your enimie(s) into a wall to kill it.
Craft 		--- Crafts a magical wall with your mouse for x seconds
Teleport 	--- Teleports you to where you want to go.


