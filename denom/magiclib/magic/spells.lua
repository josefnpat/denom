spells = {}


function spells.load()
	types = {"earth","fire","air", "water", "lightning" }
 	cd = {"north","east","south","west"} -- cardinal directions
	magiclib.img_t = {}
	magiclib.ani_t = {}
	for _,typev in ipairs(types) do
		magiclib.img_t[typev] = {}
		magiclib.ani_t[typev] = {}
  		for _,cdv in ipairs(cd) do			
			magiclib.img_t[typev][cdv] = love.graphics.newImage("magiclib/gfx/bolts/"..typev.."/"..cdv..".png")
			magiclib.ani_t[typev][cdv] = newAnimation(magiclib.img_t[typev][cdv], 32, 32, 0.2, 3)
			magiclib.ani_t[typev][cdv]:setMode("loop")
   		end
 	end	
end


