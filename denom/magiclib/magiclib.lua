-- magiclib for Denom RPG
magiclib = {}

require("magiclib/magic/spells.lua")

function magiclib.load()
  magiclib.bolt = love.graphics.newImage("magiclib/firebolt.png")
  magiclib.boltSpeed = 300
  magiclib.bolts = {}
  magiclib.bolt_cost = 5
end

function magiclib.draw()
  for i,v in ipairs(magiclib.bolts) do
    love.graphics.draw(magiclib.bolt, v.x+maplib.tx, v.y+maplib.ty)
  end
end

function magiclib.update(dt)
  for i,v in ipairs(magiclib.bolts) do
    v.x = v.x + (v.dx * dt)
    v.y = v.y + (v.dy * dt)
    for j,w in ipairs(entitylib.data) do
      if entitylib.dist(v.x,v.y,w.x,w.y) < 16 then
        w.hp = w.hp - 10
        table.remove(magiclib.bolts,i)
        if w.hp <= 0 then
          table.remove(entitylib.data,j)        
        end
      end
    end
  end
end

function magiclib.mousepressed(x, y, button)
    if button == "l" then
      if guilib.mana >= magiclib.bolt_cost then
				local startX = -maplib.tx + playerlib.x
        local startY = -maplib.ty + playerlib.y
        local mouseX = -maplib.tx + x
        local mouseY = -maplib.ty + y
       
        local angle = math.atan2((mouseY - startY), (mouseX - startX))
       
        local bulletDx = magiclib.boltSpeed * math.cos(angle)
        local bulletDy = magiclib.boltSpeed * math.sin(angle)
       	
        table.insert(magiclib.bolts, {x = startX, y = startY, dx = bulletDx, dy = bulletDy})
			  guilib.mana = guilib.mana - magiclib.bolt_cost
			end
    end
end

function magiclib.keypressed(key,unicode)

end
