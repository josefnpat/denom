npclib = {}

require("npclib/npcscripts")

function npclib.load()
	npclib.drawMessage = false
	
	
	
end

function npclib.draw()
	if npclib.drawMessage then
		msglib.draw()
	end
end

function npclib.update(dt)
	
end

function npclib.keypressed(key, unicode)
	if key == "m" then
		npclib.drawMessage = true
	elseif key == "n" then
		npclib.drawMessage = false
	end
end

function npclib.loadNpc(npctype, realname, dis, x, y, scriptLocation ) 
	npclib.npcType = npctype
	npclib.npcType.rname = realname
	npclib.npcType.dis = dis
	npclib.npcType.x = x
	npclib.npcType.y = y
	npclib.npcType.script = scriptLocation
	
end


